<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User as UserModel;
use Auth;
use Crypt;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user_data = UserModel::select('*')->where('id', Auth::user()->id)->get();
        
        foreach ($user_data as $key => $data) {

            $user_data[$key]["gender"] = "Male";
            $user_data[$key]["birthday"] = date('F d, Y');
        }

        return view('home', ['user_data' => $user_data]); 
    
    }

    public function destroy(User $user)
    {
        //

        $user->delete();
        return redirect("/login");
    }
}
