@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            @foreach($user_data as $data)
                <div class="card-header">Welcome {{$data->name}}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                  
                        <p>Your email: {{$data->email}} </p>
                        <p>Gender: {{$data->gender}} </p>
                        <p>Birthday: {{$data->birthday}} </p>
                        <p>Profile Created: {{$data->created_at->hour }} Hours Ago</p>
                    @endforeach
                    <button class="btn btn-warning">Edit Profile</button>
                    <button class="btn btn-danger">Delete Profile</button>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
