<?php

namespace App\Http\Controllers;

use App\Http\Models\MockModel as MockModel;
use Illuminate\Http\Request;

class MockDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        echo "hello!";
        exit();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\mock_data  $mock_data
     * @return \Illuminate\Http\Response
     */
    public function show(mock_data $mock_data)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\mock_data  $mock_data
     * @return \Illuminate\Http\Response
     */
    public function edit(mock_data $mock_data)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\mock_data  $mock_data
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, mock_data $mock_data)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\mock_data  $mock_data
     * @return \Illuminate\Http\Response
     */
    public function destroy(mock_data $mock_data)
    {
        //
    }

    public function fetchData() {
        
        $data = MockModel::all();
        
        return json_encode($data);
    }
}
