<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class MockModel extends Model
{
    protected $table = 'mock_datas';

    protected $primaryKey = 'id';
}
