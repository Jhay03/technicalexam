@extends("layouts.app")

@section("content")
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="https://images.pexels.com/photos/3759078/pexels-photo-3759078.jpeg?cs=srgb&dl=white-picture-frame-on-table-3759078.jpg&fm=jpg" alt="First slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="https://images.pexels.com/photos/4050387/pexels-photo-4050387.jpeg?cs=srgb&dl=woman-using-laptop-in-bed-4050387.jpg&fm=jpg" alt="Second slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="https://images.pexels.com/photos/3773405/pexels-photo-3773405.jpeg?cs=srgb&dl=laptop-computer-on-white-table-3773405.jpg&fm=jpg" alt="Third slide">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

<h1 class="text-center">List of Data</h1>
<table class="table table-striped table-bordered" id="myTable">
  <thead>
    <tr>
      <td>ID</td>
      <td>Name</td>
      <td>Gender</td>
      <td>Age</td>
    </tr>
  </thead>
	<tbody></tbody>
  <tfoot>
    <tr>
      <td>ID</td>
      <td>NAME</td>
      <td>Gender</td>
      <td>Age</td>
    </tr>
  </tfoot>
  <tbody>
</table>
@endsection