$(document).ready( function () {
    $('#myTable').DataTable({
        responsive: true,
        serverside: true,
        processing: true,
        ajax:
        {
            type: 'GET',
            url: "/data",
            dataType: 'json',
            dataSrc: ''
        },
        "columns": [
            { data: 'id' },
            { data: 'name' },
            { data: 'gender' },
            { data: 'age' },
        ]
    });
});