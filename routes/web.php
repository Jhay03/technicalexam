<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('mockdata');
});

Auth::routes();
/* Route::resource('mock', 'MockDataController'); */;

Route::resource('mock', 'MockDataController');
Route::get('/data', 'MockDataController@fetchData');
Route::get('/home', 'HomeController@index')->name('home');
