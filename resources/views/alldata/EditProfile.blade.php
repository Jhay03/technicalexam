@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit your profile</div>

                <div class="card-body">
                   <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" class="form-control" placeholder="name">
                   </div>

                   <div class="form-group">
                    <label for="name">Birthday:</label>
                    <input type="text" class="form-control" placeholder="bday">
                   </div>

                   <div class="form-group">
                    <label for="gender">Gender:</label>
                    <input type="text" class="form-control" placeholder="gender">
                   </div>
                   <button class="btn btn-primary"> Update your profile</button>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
